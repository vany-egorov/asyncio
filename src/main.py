import asyncio
import aiohttp_jinja2
import jinja2
from aiohttp import web


@asyncio.coroutine
@aiohttp_jinja2.template("index.html")
def handle(request):
    name = request.match_info.get('name', "Anonymous")
    return {"name": name}


@asyncio.coroutine
def init(loop):
    app = web.Application(loop=loop)
    app.router.add_route('GET', '/{name}', handle)

    aiohttp_jinja2.setup(
        app,
        loader=jinja2.FileSystemLoader("./templates"))

    srv = yield from loop.create_server(app.make_handler(),
                                        "0.0.0.0", 8000)
    print("Server started at http://0.0.0.0:8000")
    return srv

loop = asyncio.get_event_loop()
loop.run_until_complete(init(loop))
loop.run_forever()
